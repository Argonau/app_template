"""Custom ListWidget used in the renaming tool
"""

import os, sys
from pathlib import Path
from PyQt5 import QtCore, QtGui, QtWidgets

### for testing and display purposes: ###
import pprint
pp = pprint.PrettyPrinter(indent=4)
#########################################

class CustomListWidgetItem(QtWidgets.QListWidgetItem):
    """Sorting is alphabetical and case-insensitive by default. If you want a custom sort-order, subclass QListWidgetItem and re-implement its less-than operator"""
    ### https://stackoverflow.com/questions/22489018/pyqt-how-to-get-most-of-qlistwidget#
    def __init__(self, sorting_style="fileName"): # "fileName" , "last_mod", "fileSize", None
        super().__init__()
        self.sorting_style = sorting_style
        self.setToolTip('Right click to remove this item')

    def __lt__(self, other): 
        """Perform “rich comparisons” between a and b. Specifically, lt(a, b) is equivalent to a < b"""
        statinfo_self = os.stat(self.data(0x0100)["file"])
        statinfo_other = os.stat(other.data(0x0100)["file"])

        # return self.text() < other.text() # comparison by the item text
        if self.sorting_style == "fileName":
            return self.data(0x0100)["file"].name < other.data(0x0100)["file"].name # comparison by file name
        elif self.sorting_style == "last_mod":
            return statinfo_self.st_mtime < statinfo_other.st_mtime # comparison by last modified time-stamp
        elif self.sorting_style == "fileSize":
            return statinfo_self.st_mtime < statinfo_other.st_size # comparison by file size
        else:
            return super().__lt__(other) # returns the original QListWidgetItem __lt__ method unchanged

class CustomListWidget(QtWidgets.QListWidget):
    """ gets the files in a folder via drag an drop on a ListWidget""" 
    # QListWidget can't do drag and drop by default, therefore the following is apperently necessary to get it run

    filesSignal = QtCore.pyqtSignal(list)
    itemsSignal = QtCore.pyqtSignal(list)
    lW_FileList = []

    def __init__(self, parent):
        super().__init__(parent)
        self.setToolTip('Right click removes items from the list')
        # self.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove) # geht leider nicht
        self.setSortingEnabled(True) # enables the sorting rules. Default is false. Without that the __lt__ changes in CustomListWidgetItem class don't have any effect

        self.sort_ascending = True
        self.sortingStyleDict = {
                                "fileName": "Sorted by file name",
                                "last_mod": "Sorted by last modified date",
                                "fileSize": "Sorted by file size",
                                None: "No auto sorting",
                                }

        self.sorting_style = list(self.sortingStyleDict.keys())[0] # "fileName"
        
        self.displayFullPaths = False
        self.setAcceptDrops(True)
        self.formats = (".igs", ".wire", ".catpart", ".txt")
        
        ### alternating row colors with transparency ###
        self.setAlternatingRowColors(True);
        self.setStyleSheet("alternate-background-color: rgba(255, 255, 255, 80);") # rgb + apha 0-255

        self.lW_itemList = []

    def getAll_items(self):
        """selects all items in the listWidget and emit the list as a signal"""
        self.lW_itemList = [self.item(index) for index in range(self.count())] # select all items
        self.itemsSignal.emit(self.lW_itemList)
        return self.lW_itemList

    def mimeTypes(self):
        """extends the list of mime type to URIs"""
        mimetypes = super().mimeTypes()
        mimetypes.append('text/uri-list')
        return mimetypes

    def dropMimeData(self, index, data, action):
        """picks up drop-events. Creates a path object list form the files dropped"""
        if data.hasUrls():  # only if the data holds file-urls
            ### process dropped mimeData and create lW_FileList ###
            for i in data.urls():    # data is a class from QMimeData. data.urls() gets a list of urls, check here: http://doc.qt.io/qt-5/qurl-members.html
                # print(i)           # i is a PyQt5.QtCore.QUrl Object
                if i.url().startswith("file:///"):
                    shortUrl = i.url()[8:]  # lokale Adressen wie "file:///C:/Users/arwed.gollner" --> "C:/Users/arwed.gollner"
                else:
                    shortUrl = i.url()[5:]  # für Netwerk-Adressen: "file://10.119.0.30/nextev...." --> "//10.119.0.30/nextev...."

                if os.path.isdir(shortUrl):  # checks if data is a folder
                    folderfileList = self.walkThrough(shortUrl)  # list of path objects
                    for afile in folderfileList:
                        if (afile not in self.lW_FileList and Path(afile).suffix in self.formats):  # checks if path objects are already in the listwidget filelist and the format is supported
                            self.lW_FileList.append(afile)
                            self.create_LW_Item(afile)

                if (Path(shortUrl) not in self.lW_FileList and not os.path.isdir(shortUrl) and Path(shortUrl).suffix in self.formats): # nicht in der Liste und kein Ordner und format wird unterstützt
                    self.lW_FileList.append(Path(shortUrl))
                    self.create_LW_Item(Path(shortUrl))
                else:
                    continue

            self.filesSignal.emit(self.lW_FileList)
            self.getAll_items() # emitting the list off all items in the list widget

            # ### this filters all listwidget item-texts for the string ".wire" and sets the FontColor ###
            # items = self.findItems(".wire", QtCore.Qt.MatchEndsWith) # many different matchflags available: matchExactly, MatchContains a.s.o
            # for item in items:
            #     item.setForeground(QtGui.QColor("#02667D"))  # setTextColor is deprecated, use setForeground() instead 

            return True
        else:
            return super().dropMimeData(index, data, action)

    def create_LW_Item(self, data): # data needs to be a pathlib Path object
        """takes input data (e.g.Path-objects) and creates a listWidget item with it
           Be carefull when using this method. ListWidgetItems __lt__ can raise an error when autosorting is turned ON while trying to create an item with a different sorting style"""
        dataDict = {"file": data}
        # item = QtWidgets.QListWidgetItem() # standart item
        item = CustomListWidgetItem(sorting_style=self.sorting_style) # custom item class with alternative sorting rules
        item.setData(0x0100, dataDict)  # ItemDataRole: 0x0100 = Qt::UserRole, the first role that can be used for application-specific purposes.
        # self.lW_itemList.append(item)

        if self.displayFullPaths:
            item.setText(str(data))  # text displayed in the listWidget
        else:
            item.setText(str(data.name))  # text displayed in the listWidget
        self.addItem(item)    # adds the filename to the QListWidget
        return item

    def change_LW_item(self, item, newData):
        """changes listwidget data (e.g.Path-objects) and alters the displayed item text.
           Be carefull when using this method. ListWidgetItems __lt__ can raise an error when autosorting is turned ON while changing the item """
        self.lW_FileList.remove(item.data(0x0100)["file"]) # ItemDataRole: 0x0100 = Qt::UserRole, removes the Datadict filepath entry from the fileList
        # print("CustomListWidget, item data")
        # pp.pprint(item.data(0x0100))
        self.lW_FileList.append(Path(newData["file"])) # puts the new fileName on the fileList
        # print("CustomListWidget, Newdata")
        # pp.pprint(newData)

        item.setData(0x0100, newData)  # ItemDataRole: 0x0100 = Qt::UserRole, the first role that can be used for application-specific purposes.
        item.setText(str(newData["file"]))  # text displayed in the listWidget

        if self.displayFullPaths:
            item.setText(str(newData["file"]))  # text displayed in the listWidget
        else:
            item.setText(str(newData["file"].name))  # text displayed in the listWidget
        return item

    def removeItem(self, item):
        """removes an item from the listWidget. Returns a dictionary with the containing data of that item"""
        dataDict = item.data(0x0100)
        qIndex = self.indexFromItem(item) # gets the index of the selected item
        self.takeItem(qIndex.row())  # removes the item at a given index
        return dataDict

    @staticmethod
    def walkThrough(aFolderPath):
        """Walkes through a given folder. (not recursive, so no subfolder) Creates a list of path objects"""
        fileList = []
        for dirpath, dirnames, filenames in os.walk(aFolderPath):
            for afile in filenames:
                path = Path(dirpath).joinpath(afile)
                fileList.append(path)
            break
        return fileList

    def mouseReleaseEvent(self, event):
        """catches the mouse-release event and executes some funky stuff"""
        super().mousePressEvent(event)  # keep the functionality of that method and add my stuff to it afterwards instead of overwriting it
        
        ### adding the right-click functionality. It removes items in the listWidget ###
        if event.button() == QtCore.Qt.RightButton:
            item = self.itemAt(event.x(), event.y())
            
            if item:    # if it actually found an listWidget-item at the clicked position. 
                # print(item.data(0x0100))
                dataDict = item.data(0x0100)
                self.lW_FileList.remove(dataDict["file"]) # ItemDataRole: 0x0100 = Qt::UserRole
                self.removeItem(item) # ItemDataRole: 0x0100 = Qt::UserRole

                ### emiting changed file and item lists
                self.filesSignal.emit(self.lW_FileList)
                self.getAll_items()

    def show_fullPath(self, aBool):
        """switches the item display between fullpath and only-file-name"""
        itemList = [self.item(index) for index in range(self.count())] # select all items
        ### full path display ###
        if aBool:
            self.displayFullPaths = True
            for item in itemList:
                data = item.data(0x0100)["file"]
                item.setText(str(data))
        ### only file names display ###        
        else:
            self.displayFullPaths = False
            for item in itemList:
                data = item.data(0x0100)["file"]
                item.setText(str(data.name)) # since the data is a Pathlib Path object we can use "aPath.name" to get the filename

    def show_preview(self, item, previewText):
        item.setText(previewText)

    def set_sorting_order(self, sorting_style):
        """setter method for the custom listwidgets sorting style"""
        self.sorting_style = sorting_style

    def switch_sorting_order(self):
        """switch sorting style and rearrange list items accordingly"""
        print(f"sorting style: {self.sorting_style}")
        if not self.sorting_style:
            self.setSortingEnabled(False)
            return None
        else:
            self.setSortingEnabled(True)

        allitems = self.lW_itemList
        if allitems:
            tempDataList = [item.data(0x0100) for item in allitems]
            self.clear()

            for keepData in tempDataList:
                newItem = self.create_LW_Item(keepData["file"])
                self.change_LW_item(newItem, keepData)

        self.getAll_items() # emit signal the updated itemList

    def start_sorting(self):
        """switching back on the selfsorting in the listwidget"""
        if not self.sorting_style: # if the selfsorting style is None
            self.setSortingEnabled(False)
        else:
            self.setSortingEnabled(True)

    def stop_sorting(self):
        """switching off the selfsorting in the listWidget. __lt__ raises an Error when there CustomListItems in the widget with a different __lt__ definitions"""
        self.setSortingEnabled(False) 
##################################################################################################################################

def main():

    def switch_order():
        if myListWd.sort_ascending:
            myListWd.sort_ascending = False
            myListWd.sortItems(QtCore.Qt.DescendingOrder)
        else:
            myListWd.sort_ascending = True
            myListWd.sortItems(QtCore.Qt.AscendingOrder)

    def switch_style():
        choises = list(myListWd.sortingStyleDict.keys())
        global counter

        myListWd.set_sorting_order(choises[counter])
        myListWd.switch_sorting_order()
        button.setText(f"Switched to sorting order: {myListWd.sorting_style}")

        if counter < len(choises)-1:
            counter +=1
        else:
            counter = 0


    app = QtWidgets.QApplication(sys.argv)
    window = QtWidgets.QWidget()
    window.resize(450, 350)
    window.move(800, 300)
    window.setWindowTitle('Widget Demo')
    
    title = QtWidgets.QLabel("Demo for the custom QListWidget")
    myListWd = CustomListWidget(window)

    button = QtWidgets.QPushButton(window)
    button.setText("Switch sorting order")
    button.setMinimumSize(QtCore.QSize(120, 40))
    
    # button.clicked.connect(switch_order)
    button.clicked.connect(switch_style)

    window_layout = QtWidgets.QVBoxLayout(window) # verticalLayout
    window_layout.addWidget(title)
    window_layout.addWidget(myListWd)
    window_layout.addWidget(button)
    window.setLayout(window_layout)

    window.show()
    app.exec_()


if __name__ == '__main__':
    counter = 0
    main()