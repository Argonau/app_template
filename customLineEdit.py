"""Custom LineEdit used in the renaming tool
"""

import os, sys
from pathlib import Path
from PyQt5 import QtCore, QtGui, QtWidgets

### for testing and display purposes: ###
import pprint
pp = pprint.PrettyPrinter(indent=4)
#########################################


class CustomLineEdit(QtWidgets.QLineEdit):
    """ gets a folder path via drag in a LineEdit widget""" 

    pathSignal = QtCore.pyqtSignal(Path)

    def __init__(self, parent):
        super().__init__(parent)
        self.setToolTip('You can drop a folder from the explorer into this!')

        self.displayFullPaths = False
        self.setAcceptDrops(True)

        self.startFolder = Path()

    def dragEnterEvent( self, event ):
        data = event.mimeData()
        urls = data.urls()
        if ( urls and urls[0].scheme() == 'file' ):
            event.acceptProposedAction()

    def dragMoveEvent( self, event ):
        data = event.mimeData()
        urls = data.urls()
        if ( urls and urls[0].scheme() == 'file' ):
            event.acceptProposedAction()

    def dropEvent( self, event ):
        data = event.mimeData()
        urls = data.urls()
        if ( urls and urls[0].scheme() == 'file' ):
            # for some reason, this doubles up the intro slash
            filepath = str(urls[0].path())[1:]
            # self.startFolder = Path(filepath)
            if not os.path.isdir(filepath): # if a a file was droped and not a folder
                filepath = Path(filepath).parent
            self.setText(str(filepath))
            self.folderpath = Path(filepath)
            self.pathSignal.emit(self.folderpath)

    # def mouseReleaseEvent(self, event):
    #     """catches the mouse-release event and executes some funky stuff"""
    #     super().mousePressEvent(event)  # keep the functionality of that method and add my stuff to it afterwards instead of overwriting it
        
    #     ### adding the right-click functionality. It removes items in the listWidget ###
    #     if event.button() == QtCore.Qt.RightButton:
    #         item = self.itemAt(event.x(), event.y())
    #         qIndex = self.indexFromItem(item) # gets the index of the selected item
            
    #         if item:    # if it actually found an listWidget-item at the clicked position. 
    #             # print(item.data(0x0100))
    #             dataDict = item.data(0x0100)
    #             self.lW_FileList.remove(dataDict["file"]) # ItemDataRole: 0x0100 = Qt::UserRole
    #             self.takeItem(qIndex.row())  # removes the item at a given index
                
    #             ### emiting changed file and item lists
    #             self.filesSignal.emit(self.lW_FileList)
    #             self.getAll_items()

    def switch_fullPath(self, aBool):
        """switches the item display between fullpath and only-file-name"""
        itemList = [self.item(index) for index in range(self.count())] # select all items
        ### full path display ###
        if aBool:
            self.displayFullPaths = True
            for item in itemList:
                data = item.data(0x0100)["file"]
                item.setText(str(data))
        ### only file names display ###        
        else:
            self.displayFullPaths = False
            for item in itemList:
                data = item.data(0x0100)["file"]
                item.setText(str(data.name)) # since the data is a Pathlib Path object we can use "aPath.name" to get the filename


##################################################################################################################################

# def main():

#     def switch_order():
#         if myListWd.sort_ascending:
#             myListWd.sort_ascending = False
#             myListWd.sortItems(QtCore.Qt.DescendingOrder)
#         else:
#             myListWd.sort_ascending = True
#             myListWd.sortItems(QtCore.Qt.AscendingOrder)

#     def switch_style():
#         choises = list(myListWd.sortingStyleDict.keys())
#         global counter

#         myListWd.set_sorting_order(choises[counter])
#         myListWd.switch_sorting_order()
#         button.setText(f"Switched to sorting order: {myListWd.sorting_style}")

#         if counter < len(choises)-1:
#             counter +=1
#         else:
#             counter = 0


#     app = QtWidgets.QApplication(sys.argv)
#     window = QtWidgets.QWidget()
#     window.resize(450, 350)
#     window.move(800, 300)
#     window.setWindowTitle('Widget Demo')
    
#     title = QtWidgets.QLabel("Demo for the custom QListWidget")
#     myListWd = CustomListWidget(window)

#     button = QtWidgets.QPushButton(window)
#     button.setText("Switch sorting order")
#     button.setMinimumSize(QtCore.QSize(120, 40))
    
#     # button.clicked.connect(switch_order)
#     button.clicked.connect(switch_style)

#     window_layout = QtWidgets.QVBoxLayout(window) # verticalLayout
#     window_layout.addWidget(title)
#     window_layout.addWidget(myListWd)
#     window_layout.addWidget(button)
#     window.setLayout(window_layout)

#     window.show()
#     app.exec_()


# if __name__ == '__main__':
#     counter = 0
#     main()