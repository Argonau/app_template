"""Renaming-Tool helper: converts Ui files and the resource files from QT Designer to pyqt python files
"""

import subprocess
import os

command_UI = "pyuic5.exe design/Design.ui -o design.py"
command_Resources = "pyrcc5 resources/DesignRessources.qrc -o DesignRessources_rc.py"
cwd = os.getcwd()

subprocess.call(command_UI, shell=False, cwd=cwd)
subprocess.call(command_Resources, shell=False, cwd=cwd)

print("done")