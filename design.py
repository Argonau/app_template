# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'design/Design.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(512, 580)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMinimumSize(QtCore.QSize(512, 580))
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/Icons/GearEye.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        MainWindow.setStyleSheet("#listWidget_rn_rename{\n"
"    border: 2px solid #8f8f91;\n"
"    border-radius: 6px;\n"
"\n"
"    background-image: url(:/icons/ListWidget_Files_Icon.png);\n"
"    background-position: center;\n"
"    background-repeat: no-repeat;\n"
"\n"
"\n"
"}\n"
"")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centralwidget.sizePolicy().hasHeightForWidth())
        self.centralwidget.setSizePolicy(sizePolicy)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy)
        self.tabWidget.setObjectName("tabWidget")
        self.tab_rename = QtWidgets.QWidget()
        self.tab_rename.setObjectName("tab_rename")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.tab_rename)
        self.verticalLayout.setObjectName("verticalLayout")
        self.listWidget_rn_rename = CustomListWidget(self.tab_rename)
        self.listWidget_rn_rename.setMinimumSize(QtCore.QSize(0, 300))
        self.listWidget_rn_rename.setObjectName("listWidget_rn_rename")
        self.verticalLayout.addWidget(self.listWidget_rn_rename)
        self.comboBox_rn_presets = QtWidgets.QComboBox(self.tab_rename)
        self.comboBox_rn_presets.setObjectName("comboBox_rn_presets")
        self.verticalLayout.addWidget(self.comboBox_rn_presets)
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.gridLayout.setContentsMargins(-1, 5, -1, 6)
        self.gridLayout.setObjectName("gridLayout")
        self.lineEdit_rn_Modeler = QtWidgets.QLineEdit(self.tab_rename)
        self.lineEdit_rn_Modeler.setObjectName("lineEdit_rn_Modeler")
        self.gridLayout.addWidget(self.lineEdit_rn_Modeler, 1, 1, 1, 1)
        self.lineEdit_rn_partName = QtWidgets.QLineEdit(self.tab_rename)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.lineEdit_rn_partName.sizePolicy().hasHeightForWidth())
        self.lineEdit_rn_partName.setSizePolicy(sizePolicy)
        self.lineEdit_rn_partName.setMinimumSize(QtCore.QSize(0, 0))
        self.lineEdit_rn_partName.setObjectName("lineEdit_rn_partName")
        self.gridLayout.addWidget(self.lineEdit_rn_partName, 0, 1, 1, 1)
        self.comboBox_rn_project = QtWidgets.QComboBox(self.tab_rename)
        self.comboBox_rn_project.setObjectName("comboBox_rn_project")
        self.gridLayout.addWidget(self.comboBox_rn_project, 0, 0, 1, 1)
        self.comboBox_rn_scope = QtWidgets.QComboBox(self.tab_rename)
        self.comboBox_rn_scope.setObjectName("comboBox_rn_scope")
        self.gridLayout.addWidget(self.comboBox_rn_scope, 1, 0, 1, 1)
        self.comboBox_rn_phase = QtWidgets.QComboBox(self.tab_rename)
        self.comboBox_rn_phase.setObjectName("comboBox_rn_phase")
        self.gridLayout.addWidget(self.comboBox_rn_phase, 2, 0, 1, 1)
        self.comboBox = QtWidgets.QComboBox(self.tab_rename)
        self.comboBox.setObjectName("comboBox")
        self.gridLayout.addWidget(self.comboBox, 0, 2, 1, 1)
        self.comboBox_2 = QtWidgets.QComboBox(self.tab_rename)
        self.comboBox_2.setObjectName("comboBox_2")
        self.gridLayout.addWidget(self.comboBox_2, 1, 2, 1, 1)
        self.comboBox_3 = QtWidgets.QComboBox(self.tab_rename)
        self.comboBox_3.setObjectName("comboBox_3")
        self.gridLayout.addWidget(self.comboBox_3, 2, 2, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout)
        spacerItem = QtWidgets.QSpacerItem(20, 26, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.button_rn_undo = QtWidgets.QPushButton(self.tab_rename)
        self.button_rn_undo.setMinimumSize(QtCore.QSize(120, 60))
        self.button_rn_undo.setObjectName("button_rn_undo")
        self.horizontalLayout.addWidget(self.button_rn_undo)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.button_rename = QtWidgets.QPushButton(self.tab_rename)
        self.button_rename.setMinimumSize(QtCore.QSize(120, 60))
        self.button_rename.setObjectName("button_rename")
        self.horizontalLayout.addWidget(self.button_rename)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.comboBox_rn_presets.raise_()
        self.listWidget_rn_rename.raise_()
        self.tabWidget.addTab(self.tab_rename, "")
        self.tab_function2 = Tab_function2()
        self.tab_function2.setObjectName("tab_function2")
        self.button_f2_start = QtWidgets.QPushButton(self.tab_function2)
        self.button_f2_start.setGeometry(QtCore.QRect(130, 130, 161, 81))
        self.button_f2_start.setObjectName("button_f2_start")
        self.frame_f2 = QtWidgets.QFrame(self.tab_function2)
        self.frame_f2.setGeometry(QtCore.QRect(129, 240, 161, 121))
        self.frame_f2.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_f2.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_f2.setObjectName("frame_f2")
        self.tabWidget.addTab(self.tab_function2, "")
        self.verticalLayout_2.addWidget(self.tabWidget)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "App Template"))
        self.button_rn_undo.setText(_translate("MainWindow", "Undo"))
        self.button_rename.setText(_translate("MainWindow", "Rename"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_rename), _translate("MainWindow", "Some tab"))
        self.button_f2_start.setText(_translate("MainWindow", "Push da button"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_function2), _translate("MainWindow", "Another tab"))

from customlistwidget import CustomListWidget
from tab_function2 import Tab_function2
import DesignRessources_rc
