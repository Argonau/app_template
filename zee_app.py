"""App Template
"""

__author__ = "arwed.gollner"
__version__ = "1.0.0"  # 2018.10.24

from PyQt5 import QtWidgets, QtCore
import os, sys# We need sys so that we can pass argv to QApplication

import design # This file holds our MainWindow and all design related things
              # it also keeps events etc that we defined in Qt Designer

class AppClass(QtWidgets.QMainWindow, design.Ui_MainWindow):

    version = __version__
    formats = (".igs", ".CATPart", ".stp", ".edf", ".prt", ".fbx", ".wref", ".wire", ".vpb")    # FIXME: obj, dwg geht nicht

    def __init__(self):
        super().__init__()

        ### setup UI ###
        self.setupUi(self)  # This is defined in design.py file automatically. It sets up layout and widgets that are defined
        self.startUpUIChanges()

        ### some default start values ###
        self.listWidget_rn_rename.formats = self.formats # changing the customlistWidget formats to the ones used in this app

        ### set up a custom Tab ###
        ### don't forget the set a placeholder for the custom class in Qt designer in the design.ui file.
        self.tab_function2.appObj = self # !!!! this is necessary to give back the app object (in our case called "form"). Now all widgets get accessable in that Custom Tab class
        self.button_f2_start.clicked.connect(self.tab_function2.change_color)

    def startUpUIChanges(self):
        _translate = QtCore.QCoreApplication.translate
        self.mainTitle = "App Template " + self.version
        self.setWindowTitle(_translate("MainWindow", self.mainTitle))

    def signaltest(self, event):
        print("AppClass: ", event)


class WorkThread(QtCore.QThread):

    def __init__(self, appObjName, parent=None):  # appClass Objectname muss noch übergeben werden
        super().__init__()

        self.is_running = False
        self.setRunState(False)  # emits the signal that "self.is_running" is False

        self.appObjName = appObjName

    def signalTest(self, irgendwas):  # Funktioniert aber warum kann ich einfach "irgendwas als Argument schreiben und er zeigt die richtige Liste an?"
        """prints the signal"""
        print(irgendwas)

    def run(self):
        """runs the thread"""

        self.setRunState(True)
        print("the watcher is running")

    def stop(self):
        """stops the thread"""

        self.setRunState(False)
        print("stopping thread...")
        self.terminate()  # stopping the thread # FIXME: this is suppost to be bad. rather use stop


def main():
    app = QtWidgets.QApplication(sys.argv)  # A new instance of QApplication
    form = AppClass()                 # We set the form to be our App (design)
    form.show()                         # Show the form
    app.exec_()                         # and execute the app


if __name__ == '__main__':              # if we're running file directly and not importing it
    main()                              # run the main function
