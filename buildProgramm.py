"""AFC-Tool helper: deploys and packs the application with cx_freeze and setup.py
"""

import subprocess
import os
from pathlib import Path

command_build = "python.exe setup.py build"
dir_ = os.path.dirname(os.path.realpath(__file__))
settingsFile = Path(dir_).joinpath("settings", "settings.txt")

try:
	os.remove(settingsFile)  # if this file is present when the programm is build, the app doesn't safe changed settings anymore.
except FileNotFoundError:
	print("settings file is already erased")

finally:
	subprocess.run(command_build, shell=True, cwd=os.getcwd(), stdout=open(os.devnull, 'wb'))

print("done")